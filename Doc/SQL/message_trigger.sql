--Creating message id trigger
CREATE OR REPLACE
TRIGGER twit_messageid_trig
BEFORE INSERT on TwitMessage
FOR EACH ROW

BEGIN
	SELECT twit_seq_messageid.NEXTVAL
	INTO :new.ID
	FROM dual;
END;