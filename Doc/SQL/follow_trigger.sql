--Creating follow id trigger
CREATE OR REPLACE
TRIGGER twit_followid_trig
BEFORE INSERT on TwitFollow
FOR EACH ROW

BEGIN
	SELECT twit_seq_followid.NEXTVAL
	INTO :new.ID
	FROM dual;
END;