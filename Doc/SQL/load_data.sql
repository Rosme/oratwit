--Purging all data
--Droping Sequence
DROP SEQUENCE TWIT_SEQ_FOLLOWID;
DROP SEQUENCE TWIT_SEQ_MESSAGEID;
DROP SEQUENCE TWIT_SEQ_USERID;

--Dropping tables (index and trigger will drop automatically)
DROP TABLE TWITFOLLOW;
DROP TABLE TWITMESSAGE;
DROP TABLE TWITUSERS;


--Creating tables (load files from J: (work only for JS, adapt path for others))

@ J:\GitLocal\OraTwit\Doc\SQL\table_sequence.sql
@ J:\GitLocal\OraTwit\Doc\SQL\index.sql
@ J:\GitLocal\OraTwit\Doc\SQL\follow_trigger.sql
@ J:\GitLocal\OraTwit\Doc\SQL\message_trigger.sql
@ J:\GitLocal\OraTwit\Doc\SQL\user_trigger.sql


--Loading fake data

--Creating users
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('macha', 'Mathieu', 'Charlebois', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('mocromato', 'Vincent', 'Robert', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('alphamaja', 'Val�rie', 'L�vesque', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('lavala', 'Michelle', 'Lamontagne', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('natsam', 'Nathan', 'Tremblay', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('crosmos', 'Audrey', 'Carignan', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('netso', 'Nathalie', 'Somberlo', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('repidramo', 'R�mi', 'Dar�che', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('vastomi', 'Val�rie', 'Robert', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('fasoli', 'Vincent', 'Charlebois', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8');
commit;

--Making the following
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), (SELECT id FROM twitusers WHERE username = 'macha'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), (SELECT id FROM twitusers WHERE username = 'mocromato'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), (SELECT id FROM twitusers WHERE username = 'alphamaja'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'lavala'), (SELECT id FROM twitusers WHERE username = 'lavala'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), (SELECT id FROM twitusers WHERE username = 'natsam'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), (SELECT id FROM twitusers WHERE username = 'crosmos'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), (SELECT id FROM twitusers WHERE username = 'repidramo'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'vastomi'), (SELECT id FROM twitusers WHERE username = 'vastomi'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), (SELECT id FROM twitusers WHERE username = 'fasoli'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'netso'), (SELECT id FROM twitusers WHERE username = 'netso'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), (SELECT id FROM twitusers WHERE username = 'mocromato'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), (SELECT id FROM twitusers WHERE username = 'lavala'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), (SELECT id FROM twitusers WHERE username = 'natsam'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), (SELECT id FROM twitusers WHERE username = 'netso'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), (SELECT id FROM twitusers WHERE username = 'macha'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), (SELECT id FROM twitusers WHERE username = 'alphamaja'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), (SELECT id FROM twitusers WHERE username = 'crosmos'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), (SELECT id FROM twitusers WHERE username = 'vastomi'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), (SELECT id FROM twitusers WHERE username = 'macha'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), (SELECT id FROM twitusers WHERE username = 'vastomi'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), (SELECT id FROM twitusers WHERE username = 'fasoli'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'lavala'), (SELECT id FROM twitusers WHERE username = 'alphamaja'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'lavala'), (SELECT id FROM twitusers WHERE username = 'fasoli'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), (SELECT id FROM twitusers WHERE username = 'mocromato'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), (SELECT id FROM twitusers WHERE username = 'lavala'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), (SELECT id FROM twitusers WHERE username = 'netso'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), (SELECT id FROM twitusers WHERE username = 'macha'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), (SELECT id FROM twitusers WHERE username = 'vastomi'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'netso'), (SELECT id FROM twitusers WHERE username = 'mocromato'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'netso'), (SELECT id FROM twitusers WHERE username = 'alphamaja'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), (SELECT id FROM twitusers WHERE username = 'macha'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), (SELECT id FROM twitusers WHERE username = 'lavala'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), (SELECT id FROM twitusers WHERE username = 'vastomi'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'vastomi'), (SELECT id FROM twitusers WHERE username = 'fasoli'));
INSERT INTO twitfollow(id_follower, id_following) 
    VALUES((SELECT id FROM twitusers WHERE username = 'vastomi'), (SELECT id FROM twitusers WHERE username = 'macha'));
commit;

--Adding tweets
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'macha'), 'Vient juste de s''inscrire sur OraTwit...c''est g�nial', to_date('2013/03/04 5:10:20', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), 'En cours...tellement plate!', to_date('2013/03/06 14:33:10', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'mocromato'), 'Enfin de retour chez moi, tellement mieux que l''�cole', to_date('2013/03/06 18:04:47', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), 'Allez pas suivre lavala, c''est vraiment juste une...', to_date('2013/03/08 10:14:22', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), 'Vraiment l�, lavala n''est qu''une...', to_date('2013/03/08 10:20:21', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), 'Pfff, lavala ne vaut rien!', to_date('2013/03/08 10:22:14', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'alphamaja'), 'lavala...la pire personne au monde', to_date('2013/03/08 10:25:16', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'lavala'), 'alphamaja, dit toujours autant n''importe quoi!', to_date('2013/03/08 10:15:16', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'lavala'), 'L''ironie, c''est qu''alphamaja est pire que moi...', to_date('2013/03/08 10:21:46', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), 'En route pour le travail!', to_date('2013/03/12 8:10:20', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), 'Arriv� au travail, voyons voir la journ�e', to_date('2013/03/12 8:17:33', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), 'Pause diner, beaucoup de travail aujourd''hui', to_date('2013/03/12 12:00:11', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), 'Pause diner fini, on recommence � travailler', to_date('2013/03/12 13:02:22', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'natsam'), 'Fini de travailler, direction la maison!', to_date('2013/03/12 18:31:17', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), 'Ma nouvelle carte graphique est juste merveilleuse!', to_date('2013/03/18 11:17:04', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), 'Bient�t, je vais me commander mon SSD', to_date('2013/03/18 11:18:26', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'crosmos'), 'SSD commander, je pouvais pas attendre', to_date('2013/03/18 11:21:37', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'netso'), 'J''ai un nouveau site web...ou pas!', to_date('2013/03/22 15:37:54', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'netso'), 'Quel h�bergeur je devrais prendre?', to_date('2013/03/22 13:07:34', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), 'Guru...GURUUUUUUUUUUUU', to_date('2013/03/26 23:48:12', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), 'Oh, j''ai bu trop de GURUUU, pas capable de dormir!', to_date('2013/03/27 2:21:39', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'repidramo'), 'Presque pas dormi, tellement p�nible ce matin', to_date('2013/03/27 6:08:56', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'vastomi'), 'Quel universit� je vais? Tellement dur de choisir!', to_date('2013/04/02 20:01:08', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), 'N''oubliez pas, mon prochain spectacle est le 28 avril!', to_date('2013/04/06 11:34:17', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), 'On vient de me confirmez qu''il n''y a plus de place le 28 avril!', to_date('2013/04/07 15:11:35', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), 'Donc je vais en faire un autre, le 29 avril!', to_date('2013/04/07 15:13:21', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), 'Je suis tellement chanceux d''avoir autant de fan! MERCI!!!', to_date('2013/04/07 18:28:31', 'yyyy/mm/dd hh24:mi:ss'));
INSERT INTO twitmessage(id_user, message, publication_date) 
    VALUES((SELECT id FROM twitusers WHERE username = 'fasoli'), 'Sans vous mes fans, je ne serais rien!', to_date('2013/04/07 19:02:33', 'yyyy/mm/dd hh24:mi:ss'));
commit;