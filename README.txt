   ________ __________    __________________      __.______________
   \_____  \\______   \  /  _  \__    ___/  \    /  \   \__    ___/
    /   |   \|       _/ /  /_\  \|    |  \   \/\/   /   | |    |   
   /    |    \    |   \/    |    \    |   \        /|   | |    |   
   \_______  /____|_  /\____|__  /____|    \__/\  / |___| |____|   
           \/       \/         \/               \/                 

Marc Olivier Bergeron, Jean-S�bastien Fauteux, Marc Grenier, Samuel Ryc
Date: H13

[Fichiers]

Les fichiers SQL importants se trouvent dans DOC/SQL.

[Installation]
Pour l'installation, on doit modifier le chemin des fichiers SQL dans le fichier load_data.sql
On indique le chemin qui va pointer au dossier DOC/SQL.
Par exemple: C:/Travail/Oratwit/DOC/SQL si le projet Oratwit se trouve dans C:/Travail/Oratwit
On doit ex�cuter le fichier load_data.sql apr�s avoir ajust� le chemin.

Note: S'il est ex�cut� avec SQL PLUS, on doit taper un slash ('/') suivi du retour de chariot pour continuer l'ex�cution du script apr�s chaque trigger. C'est un bug de SQL PLUS sur le chargement des Triggers.

[Utilisation]
Le fichier load_data charge dans la base de donn�es plusieurs usag�s avec des Tweets ainsi que des gens qu'il suit et qui le suive.

Pour d�marrer l'application, il suffit de double cliquer sur le fichier main.py (ou de lancer l'application via l'invite de commande).

Vous pouvez utiliser un des utilisateurs suivant:
-macha
-mocromato
-alphamaja
-lavala
-natsam
-crosmos
-netso
-repidramo
-vastomi
-fasoli

Le mot de passe de tous ces utilisateurs est: 'a'

Vous avez aussi la possibilit� de vous cr�er un utilisateur. Le mot de passe de votre usager doit �tre d'au moins 8 caract�res.

� la gauche de l'�cran principal se trouve plusieurs boutons de contr�les.
Ces boutons sont:
-Home
-Tweet
-Follower
-Following
-Modifier Profil
-Recherche
-Composer un tweet
-D�connexion
-Actualiser

Home: C'est votre page d'accueil. Il affiche vos informations de bases et contient la liste de tous les tweets des gens que vous suivez ainsi que de tous vos tweets.

Tweet: Contient la liste de tous les tweets des gens que vous suivez.

Follower: Contient la liste des gens qui vous suivent.

Following: Contient la liste des gens que vous suivez.

Modifier profil: Permet de changer vos informations personnelles.

Recherche: Vous permet de rechercher par nom, pr�nom ou nom d'usager n'importe quelle autre personne.

Composer un tweet (et le gros carr� blanc): Vous pouvez taper votre message dans le carr� blanc et cliquer sur le bouton Composer un tweet pour publier votre tweet.

D�connexion: Vous d�connecte de l'application et vous ram�ne � la page de login.

Actualiser: Actualise la liste des tweets pour voir les nouveaux tweets.


Pour suivre un usager, ou arr�ter de le suivre:
Il vous suffit d'aller sur la page de l'usager que vous voulez et de cliquer sur le bouton Follow/Unfollow.

Pour aller sur la page de l'usager, il vous suffit de cliquer sur le nom de l'usager (dans n'importe quel page).


Modifier/Supprimer un tweet:
� la droite de vos tweets, il y a deux boutons: M et X
Le bouton M vous permet de modifier votre tweet.

Le bouton X permet de supprimer votre tweet.


Pour supprimer son compte:
Sur la page Modifier profil, il y a un bouton supprimer. Celui-ci d�truira de fa�on permanente votre compte. � utiliser avec pr�cautions.