# -*- coding: utf-8 -*-
import cx_Oracle
import hashlib
import datetime
from Constants import *

class Modele():
	def __init__(self,parent, connection):
		self.parent= parent
		self.user = dict()
		self.connection = connection[0]
		self.cursor = connection[1]

	def addFollowingUser(self, followingID):
		try:
			self.cursor.execute("INSERT INTO twitfollow(id_follower, id_following) VALUES(:arg_1, :arg_2)", arg_1 = self.user["id"], arg_2 = followingID)
			self.connection.commit()
		except Exception as e:
			print(e)

	def authenticate(self, username, password):
		try:
			username = username.lower()
			nPassword = hashlib.sha1(password.encode()).hexdigest()
			self.cursor.execute("SELECT id, username FROM TwitUsers WHERE username = :arg_1 AND password = :arg_2", arg_1 = username, arg_2 = nPassword)
			
			for col1, col2 in self.cursor.fetchall():
				self.user["id"] = col1
				self.user["username"] = col2
				return True
				
			return False
		except Exception as e:
			return e
	
	def checkDatabase(self):
		f = open('table_sequence.sql')
		full_sql = f.read()
		sql_commands = full_sql.split(';')

		for sql_command in sql_commands:
			try:
				self.cursor.execute(sql_command)
			except cx_Oracle.DatabaseError as e:
				error, = e.args
				if error.code == 955:
					print('Table already exists')


	def createUser(self, username, firstname, lastname, password):
		if(username == None or firstname == None or lastname == None or password == None):
			return False

		if(len(username) < 3):
			message = "Le nom d'usager doit avoir au moins 3 caractères";
			self.parent.setCreateUserErrors(message)
			return False

		if(len(password) < 8):
			message = "Le mot de passe doit avoir au moins 8 caractères";
			self.parent.setCreateUserErrors(message)
			return False
			
		try:
			username = username.lower()
			nPassword = hashlib.sha1(password.encode()).hexdigest()
			self.cursor.execute("INSERT INTO TwitUsers(username, first_name, last_name, password) VALUES(:arg_1, :arg_2, :arg_3, :arg_4)", arg_1 = username, arg_2 = firstname, arg_3 = lastname, arg_4 = nPassword)
			self.connection.commit()
			
			self.parent.authenticate(username, password);
			self.addFollowingUser(self.user["id"])
			return True
		except Exception as e:
			message = "L'usager existe déjà";
			self.parent.setCreateUserErrors(message)
			return False
	
	def deleteTweet(self, id):
		try:
			self.cursor.execute("DELETE FROM TwitMessage WHERE id = :arg_1", arg_1 = str(id))
			self.connection.commit()
		except Exception as e:
			print(e)

	def deleteUser(self):
		try:
			self.cursor.execute("DELETE FROM TwitUsers WHERE id = :arg_1", arg_1 = str(self.user["id"]))
			self.connection.commit()
		except Exception as e:
			print(e)
	def disconnectUser(self):
		self.user = dict()

	def getAllTweets(self, userId):
		try:
			if(userId == None):
				userId = self.user["id"]

			self.cursor.execute("""
				SELECT DISTINCT tm.id, tm.id_user, tu2.username, tu2.first_name, tu2.last_name, tm.message, tm.publication_date
			    FROM twitusers tu1, twitusers tu2, twitfollow tf, twitmessage tm
			    WHERE tu1.id = tf.id_follower AND tu2.id = tf.id_following
			    AND tu1.id = (SELECT id FROM twitusers WHERE username = :arg_1)
			    AND tu2.id = tm.id_user
			    ORDER BY tm.publication_date DESC
				""", arg_1 = self.user["username"])
			
			result = list()
			i = 0
			for col1, col2, col3, col4, col5, col6, col7 in self.cursor.fetchall():
				result.append(dict())
				result[i]["id"] = col1
				result[i]["id_user"] = col2
				result[i]["username"] = col3
				result[i]["firstname"] = col4
				result[i]["lastname"] = col5
				result[i]["message"] = col6
				result[i]["date"] = col7
				i+=1
				
			return result
		except Exception as e:
			print(e)


	def getFollowerUsername(self):
		try:
			self.cursor.execute("""
				SELECT DISTINCT tu2.id, tu2.username, tu2.first_name, tu2.last_name, tu2.biography 
			    FROM twitusers tu1, twitusers tu2, twitfollow tf 
			    WHERE tu1.id = :arg_1 
			    AND tu2.id = tf.id_follower
			    AND tu1.id = tf.id_following
			    AND tf.id_follower != tf.id_following
				""", arg_1 = self.user["id"])
			
			result = list()
			i = 0
			for col1, col2, col3, col4, col5 in self.cursor.fetchall():
				result.append(dict())
				result[i]["id"] = col1
				result[i]["username"] = col2
				result[i]["firstname"] = col3
				result[i]["lastname"] = col4
				result[i]["biography"] = col5
				i+=1
				
			return result
		except Exception as e:
			print(e)

	def getFollowingUsername(self):
		try:
			self.cursor.execute("""
				SELECT DISTINCT tu2.id, tu2.username, tu2.first_name, tu2.last_name, tu2.biography 
			    FROM twitusers tu1, twitusers tu2, twitfollow tf 
			    WHERE tu1.id = tf.id_follower 
			    AND tu2.id = tf.id_following 
			    AND tu1.id = :arg_1
			    AND tf.id_follower != tf.id_following
				""", arg_1 = self.user["id"])
			
			result = list()
			i = 0
			for col1, col2, col3, col4, col5 in self.cursor.fetchall():
				result.append(dict())
				result[i]["id"] = col1
				result[i]["username"] = col2
				result[i]["firstname"] = col3
				result[i]["lastname"] = col4
				result[i]["biography"] = col5
				i+=1
				
			return result
		except Exception as e:
			print(e)

	def getFollowingMessages(self):
		try:
			self.cursor.execute("""
				SELECT DISTINCT tm.id, tm.id_user, tu2.username, tu2.first_name, tu2.last_name, tm.message, tm.publication_date
			    FROM twitusers tu1, twitusers tu2, twitfollow tf, twitmessage tm
			    WHERE tu1.id = tf.id_follower AND tu2.id = tf.id_following
			    AND tu1.id = (SELECT id FROM twitusers WHERE username = :arg_1)
			    AND tu2.id = tm.id_user
			    AND tu2.id != tu1.id
			    ORDER BY tm.publication_date DESC
				""", arg_1 = self.user["username"])
			
			result = list()
			i = 0
			for col1, col2, col3, col4, col5, col6, col7 in self.cursor.fetchall():
				result.append(dict())
				result[i]["id"] = col1
				result[i]["id_user"] = col2
				result[i]["username"] = col3
				result[i]["firstname"] = col4
				result[i]["lastname"] = col5
				result[i]["message"] = col6
				result[i]["date"] = col7
				i+=1
				
			return result
		except Exception as e:
			print(e)
	
	def getId(self):
		return self.user["id"]
	
	def getUserInfo(self, userId):
		try:
			if(userId == None):
				userId = self.user["id"]
				
			self.cursor.execute("""
				SELECT username, first_name, last_name, biography, sex, age, website,
			       country, email, profile_picture
			    FROM twitusers
			    WHERE id = :arg_1
				""", arg_1 = userId)
			
			result = dict()
			for col1, col2, col3, col4, col5, col6, col7, col8, col9, col10 in self.cursor.fetchall():
				result["username"] = col1
				result["firstname"] = col2
				result["lastname"] = col3
				result["biography"] = col4
				result["sex"] = col5
				result["age"] = col6
				result["website"] = col7
				result["country"] = col8
				result["email"] = col9
				result["profile_picture"] = col10
				
			return result
		except Exception as e:
			print(e)

	def getUserTweet(self, username):
		try:
			username = username.lower()
			self.cursor.execute("""
				SELECT tu.username, tm.message, tm.publication_date
			    FROM twitusers tu, twitmessage tm
			    WHERE tu.id = tm.id_user
			    AND tu.id = (SELECT id FROM twitusers WHERE username = :arg_1)
			    ORDER BY tm.publication_date DESC
				""", arg_1 = self.user["username"])
			
			result = list()
			i = 0

			for col1, col2, col3 in self.cursor.fetchall():
				result.append(dict())
				result[i]["username"] = col1
				result[i]["message"] = col2
				result[i]["date"] = col3
				i+=1
				
			return result
		except Exception as e:
			print(e)

	def isFollowing(self, userId):
		try:
			self.cursor.execute("""
				SELECT id FROM twitfollow
				WHERE id_follower = :arg_1
				AND id_following = :arg_2
				""", arg_1 = self.user["id"], arg_2 = userId)

			if len(self.cursor.fetchall()) > 0:
				return True
			else:
				return False
		except Exception as e:
			print(e)

	def modifyTweet(self, id, msg):
		try:
			self.cursor.execute("UPDATE twitmessage SET message = :arg_1 WHERE id = :arg_2", arg_1 = msg, arg_2 = id)
			self.connection.commit()
		except Exception as e:
			print(e)

	def searchUser(self, sText):
		try:
			text = sText.split(" ")
			
			if len(text) == 1:
				self.cursor.execute("SELECT id, username, first_name, last_name, biography FROM TwitUsers WHERE upper(username) like upper(:arg_1) OR upper(first_name) like upper(:arg_1) OR upper(last_name) like upper(:arg_1)", arg_1 = '%'+text[0]+'%')
			else:
				self.cursor.execute("SELECT id, username, first_name, last_name, biography FROM TwitUsers WHERE upper(username) like upper(:arg_1) OR upper(first_name) like upper(:arg_1) AND upper(last_name) like upper(:arg_2)", arg_1 = '%'+text[0]+'%', arg_2 = '%'+text[1]+'%')
			
			result = list()
			i = 0

			for col1, col2, col3, col4, col5 in self.cursor.fetchall():
				result.append(dict())
				result[i]["id"] = col1
				result[i]["username"] = col2
				result[i]["firstname"] = col3
				result[i]["lastname"] = col4
				result[i]["biography"] = col5
				i+=1
				
			return result
		except Exception as e:
			print(e)
		
	def sendReply(self, twitID, msg):
		try:
			self.cursor.execute("INSERT INTO twitmessage(id_user, message, publication_date, id_reply) VALUES(:arg_1, :arg_2, to_date('2013/03/28', 'yyyy/mm/dd'), :arg_3)", arg_1 = self.user["id"], arg_2 = msg, arg_3 = twitID)
			self.connection.commit()
		except Exception as e:
			print(e)

	def sendTweet(self, msg):
		try:
			now = datetime.datetime.now()
			date = str(now.year) + '/' + str(now.month) + '/' + str(now.day) + ' ' + str(now.hour) + ':' + str(now.minute) + ':' + str(now.second)
			self.cursor.execute("INSERT INTO twitmessage(id_user, message, publication_date) VALUES(:arg_1, :arg_2, to_date(:arg_3, 'yyyy/mm/dd hh24:mi:ss'))", arg_1 = self.user["id"], arg_2 = msg, arg_3= date)
			self.connection.commit()
		except Exception as e:
			print(e)

	def unfollow(self,followingId):
		try:
			self.cursor.execute("DELETE FROM TwitFollow WHERE id_follower = :arg_1 AND id_following = :arg_2", arg_1 = self.user["id"], arg_2 = followingId)
			self.connection.commit()
		except Exception as e:
			print(e)
			
	def updateUser(self,user):
		try:
			nPassword = hashlib.sha1(user["password"].encode()).hexdigest()
			self.cursor.execute("""
				UPDATE twitusers
				SET first_name=:arg_1,last_name=:arg_2,biography=:arg_3,SEX=:arg_4,
					age=:arg_5,website=:arg_6,profile_picture=:arg_7, country=:arg_8, email=:arg_9, password=:arg_10 
				WHERE id = (SELECT id FROM twitusers WHERE username = :arg_11)
				""", arg_1 = user["firstname"], arg_2 = user["lastname"], arg_3 = user["biography"], arg_4 = user["sex"], 
				arg_5 = user["age"], arg_6 = user["website"], arg_7 = user["profile_picture"], arg_8 = user["country"],
				arg_9 = user["email"], arg_10 = nPassword, arg_11 = self.user["username"])
			self.connection.commit()
		except Exception as e:
			print(e)