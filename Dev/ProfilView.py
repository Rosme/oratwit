 # -*- coding: iso-8859-15 -*-
from tkinter import * 
from tkinter import filedialog
import tkinter.messagebox as messagebox

class ProfilView:
	def __init__(self,parent):
		self.parent = parent
	
	def display(self):
		self.frameProfil = Frame(self.parent.tweetContainerFrame,width=800,height=740)
		
		self.frameProfil.grid_columnconfigure(0,pad=10)
		self.frameProfil.grid_columnconfigure(1,pad=10)
		
		self.frameProfil.pack()
		
	def addProfilInfo(self,user):
		user = self.noneToEmpty(user)
	
		self.usernameEntry = self.createEntry(0,"Nom d'usager : ",user["username"])
		self.usernameEntry.config(state=DISABLED)
		self.nomEntry = self.createEntry(1,"Nom : ",user["lastname"])
		self.prenomEntry = self.createEntry(2,"Prenom : ",user["firstname"])
		self.passwordEntry = self.createEntry(3,"Mot de passe : ","")
		self.passwordEntry.config(show="*")
		
		Label(self.frameProfil, text ="Biographie : ").grid(row=4,column=0,sticky=W)
		self.bioText = Text(self.frameProfil,width=25,height=5)
		self.bioText.insert(1.0,user["biography"])
		self.bioText.grid(row=4,column=1,sticky=W,padx=10)

		self.v= StringVar()

		Label(self.frameProfil, text ="Sexe : ").grid(row=5,column=0,sticky=W)
		frame = Frame(self.frameProfil)
		frame.grid(row=5,column=1,sticky=W,padx=10)
		radio = Radiobutton(frame, text="H",variable=self.v,value='H')
		radio.pack(side=LEFT)
		radio1 = Radiobutton(frame, text="F",variable=self.v,value='F')
		radio1.pack(side=LEFT)
		
		if user["sex"] == "H":
			radio.select()
		elif user["sex"] == "F":
			radio1.select()
		
		self.ageEntry = self.createEntry(6,"Age : ",user["age"])
		self.siteEntry = self.createEntry(7,"Site web",user["website"])
		self.paysEntry = self.createEntry(8,"Pays : ",user["country"])
		self.emailEntry = self.createEntry(9,"email",user["email"])
		
		#Label(self.frameProfil, text ="image").grid(row=10,column=0,sticky=W)
		#Button(self.frameProfil, text="Choisir une image",command=self.newImage).grid(row=10,column=1,sticky=W)
		
		Button(self.frameProfil, text="Sauvegarder",command=self.updateProfil).grid(row=11,column=1,sticky=E)
		Button(self.frameProfil, text="Supprimer",command=self.deleteProfil).grid(row=12,column=0,pady=10,padx=5)
		
	def hide(self):
		self.frameProfil.destroy()
	
	def createEntry(self,ligne,texte,info):
		Label(self.frameProfil, text =texte).grid(row=ligne,column=0,sticky=W,pady=5)
		temp = Entry(self.frameProfil,width=30)
		temp.insert(0,info)
		temp.grid(row=ligne,column=1,sticky=W,padx=10)
		
		return temp
	
	def newImage(self):
		#filePath = filedialog.askopenfilename(filetypes = [('GIF Files', '.gif')])
		pass
		
	def updateProfil(self):
		user = dict()
		user["username"] = self.usernameEntry.get()
		user["lastname"] = self.nomEntry.get()
		user["firstname"] = self.prenomEntry.get()
		user["password"] = self.passwordEntry.get()
		user["biography"] = self.bioText.get(1.0,END)
		user["sex"] = self.v.get()
		user["age"] = self.ageEntry.get()
		user["website"] = self.siteEntry.get()
		user["country"] = self.paysEntry.get()
		user["email"] = self.emailEntry.get()
		user["profile_picture"] = None
		
		user = self.emptyToNone(user)
		
		if self.parent.updateUser(user) == False:
			messagebox.showwarning("Erreur", "Champs invalide")
		else:
			messagebox.showinfo("OraTwit", "Modification effectu�e")
	
	def deleteProfil(self):
		if messagebox.askyesno("SUPPRESSION COMPTE","�tes-vous certain de vouloir supprimer votre compte?")== True:
			self.parent.parent.parent.deleteUser()
			self.parent.parent.hideTwit()
			self.parent.parent.displayLogin()
	
	def emptyToNone(self,user):
		
		for i in user:
			if user[i] == "":
				user[i]=None
			
		return user
		
	def noneToEmpty(self,user):
		for i in user:
			if user[i] == None:
				user[i]=""
				
		return user