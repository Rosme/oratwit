from tkinter import * 
import tkinter.messagebox as messagebox

class LoginWindow:
	def __init__(self,parent):
		self.parent = parent
		self.mainLoginFrame = Frame(self.parent.root,width=394,height=768)
		self.buildInterface()
		
	def buildInterface(self):		
		Label(self.mainLoginFrame, text="Login Twitter", font=("Arial","16","bold")).grid(column=0,row=0, pady=(0, 20))
		
		self.loginFrame = Frame(self.mainLoginFrame,width=346,height=222)
		self.loginFrame.grid(column=0,row=1)
		
		Label(self.loginFrame,text="Nom d'usager :").grid(column=0,row=0, sticky=E)
		self.usernameEntry= Entry(self.loginFrame)
		self.usernameEntry.grid(column=1,row=0,pady=4)
		self.usernameEntry.focus_set()
		
		Label(self.loginFrame,text="Mot de passe :").grid(column=0,row=1)
		self.passwordEntry= Entry(self.loginFrame,show="*")
		self.passwordEntry.grid(column=1,row=1,pady=4)
		
		self.connectButton = Button(self.loginFrame,text="Connexion",command=self.connexion)
		self.connectButton.grid(column=1,row=3, sticky=E)
		
		self.frameRegister = Frame(self.mainLoginFrame,width=346,height=340)
		self.frameRegister.grid(column=0,row=2)
		
		Label(self.frameRegister,text="S'inscrire",font=("Arial","16","bold")).grid(column=0,row=0,columnspan=2,sticky=S, pady=(20, 20))
		
		Label(self.frameRegister,text="Nom d'usager :").grid(column=0,row=1,sticky=E)
		self.usernameRegisterEntry= Entry(self.frameRegister)
		self.usernameRegisterEntry.grid(column=1,row=1,pady=4)
		
		Label(self.frameRegister,text="Prenom :").grid(column=0,row=2,sticky=E)
		self.firstNameEntry= Entry(self.frameRegister)
		self.firstNameEntry.grid(column=1,row=2,pady=4)
		
		Label(self.frameRegister,text="Nom :").grid(column=0,row=3,sticky=E)
		self.lastnameEntry= Entry(self.frameRegister)
		self.lastnameEntry.grid(column=1,row=3,pady=4)
		
		Label(self.frameRegister,text="Mot de Passe :").grid(column=0,row=4,sticky=E)
		self.passwordRegisterEntry= Entry(self.frameRegister,show="*")
		self.passwordRegisterEntry.grid(column=1,row=4,pady=4)
		
		self.registerButton = Button(self.frameRegister,text="S'inscrire",command=self.register)
		self.registerButton.grid(column=1,row=5,sticky=E)
	
	def display(self):
		self.mainLoginFrame.pack(padx=20,pady=20)
		
	def hide(self):
		self.mainLoginFrame.pack_forget()
		
	def connexion(self):
		if self.parent.parent.authenticate(self.usernameEntry.get(), self.passwordEntry.get()) == False:
			messagebox.showwarning("Erreur", "Nom d'utilisateur/mot de passe invalide")
			self.passwordEntry.delete(0,END)
		else:
			self.hide()
			self.parent.displayTwit()
			
	def register(self):
		if self.parent.parent.createUser(self.usernameRegisterEntry.get(), self.firstNameEntry.get(), self.lastnameEntry.get(), self.passwordRegisterEntry.get()) == False:
			if(self.parent.parent.getCreateUserErrors() == None):
				messagebox.showwarning("Erreur", "Champs invalide/vide")
			else:
				messagebox.showwarning("Erreur", self.parent.parent.getCreateUserErrors())

			self.usernameRegisterEntry.delete(0,END)
			self.firstNameEntry.delete(0,END)
			self.lastnameEntry.delete(0,END)
			self.passwordRegisterEntry.delete(0,END)
		else:
			self.hide()
			self.parent.displayTwit()
		
		