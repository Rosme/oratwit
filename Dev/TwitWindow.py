from tkinter import * 
import HomeView
import TweetView
import FollowView
import ProfilView
import SearchView
import HomeProfilView

class TwitWindow:
	def __init__(self,parent):
		self.parent = parent
		self.parent.root.title("Twitter")
		self.buildInterface()
		self.homeView = HomeView.HomeView(self)
		self.tweetView = TweetView.TweetView(self)
		self.followView = FollowView.FollowView(self)
		self.profilView = ProfilView.ProfilView(self)
		self.searchView  = SearchView.SearchView(self)
		self.homeProfilView = HomeProfilView.HomeProfilView(self)
		
	def display(self):
		self.showHome()
		self.mainTwitFrame.pack(fill=BOTH,expand=1)
		
	def hide(self):
		self.mainTwitFrame.pack_forget()

	def buildInterface(self):
		self.mainTwitFrame = Frame(self.parent.root,width=1024,height=768)
		
		self.leftFrame = Frame(self.mainTwitFrame,bg='grey')
		self.leftFrame.pack(side=LEFT,fill=Y,padx=10,pady=10)
		
		self.pageChoiceFrame = Frame(self.leftFrame,bd=2, bg='grey')
		self.pageChoiceFrame.pack(fill=Y)
		
		self.pageChoiceFrame.grid_columnconfigure(0,minsize=168)
		
		homeButton = Button(self.pageChoiceFrame,text="Home",command=self.showHome,width=15,bg='blue',fg='white')
		homeButton.grid(column=0,row=0,sticky=N,pady=5)
		
		tweetButton = Button(self.pageChoiceFrame,text="Tweet",command=self.showTweet,width=15,bg='blue',fg='white')
		tweetButton.grid(column=0,row=1,sticky=N,pady=5)
		
		followerButton = Button(self.pageChoiceFrame,text="Follower",command=self.showFollower,width=15,bg='blue',fg='white')
		followerButton.grid(column=0,row=2,sticky=N,pady=5)
		
		followingButton = Button(self.pageChoiceFrame,text="Following",command=self.showFollowing,width=15,bg='blue',fg='white')
		followingButton.grid(column=0,row=3,sticky=N,pady=5)
		
		profilButton = Button(self.pageChoiceFrame,text="Modifier profil",width=15,bg='blue',fg='white',command=self.showProfil)
		profilButton.grid(column=0,row=4,sticky=N,pady=5)
		
		searchButton = Button(self.pageChoiceFrame,text="Recherche",width=15,bg='blue',fg='white',command=self.showSearch)
		searchButton.grid(column=0,row=5,sticky=N,pady=5)
		
		self.textTweet = Text(self.pageChoiceFrame,width=10,height=8)
		self.textTweet.grid(column=0,row=6,sticky=N+S+E+W,pady=5,padx=5)
		
		self.textTweet.bind("<Key>",self.checkLength)
		
		newTweet = Button(self.pageChoiceFrame,text="Composer un tweet",command=self.tweet,width=18,bg='blue',fg='white')
		newTweet.grid(column=0,row=7,sticky=N+E,pady=5,padx=10)
		
		disconnectButton = Button(self.leftFrame,text="Deconnexion",width=15,bg='black',fg='red',command=self.disconnect)
		disconnectButton.pack(pady=5)
		
		self.refreshButton = Button(self.leftFrame,text="Actualiser",width=15,bg='blue',fg='white',command=self.update)
		self.refreshButton.pack(pady=20,side=BOTTOM)
		
		self.tweetContainerFrame = Frame(self.mainTwitFrame,relief=GROOVE,bd=5)
		
		self.tweetContainerFrame.pack(side=LEFT,pady=10,padx=5,fill=BOTH,expand=1)
	
	def update(self):
		etat = self.parent.state
		
		user = self.parent.parent.getUserInfo(None)
		
		if etat == "home":
			listTweet = self.parent.parent.getAllTweets(None)
			self.homeView.update()
			self.homeView.addContent(listTweet,user)
		elif etat == "tweet":
			listTweet = self.parent.parent.getFollowingMessages(None)
			self.tweetView.update()
			self.tweetView.addTweets(listTweet,user)
		elif etat == "follower":
			listUser = self.parent.parent.getFollower()
			self.followView.update()
			self.followView.addFollow(listUser)
		elif etat == "following":
			listUser = self.parent.parent.getFollowing()
			self.followView.update()
			self.followView.addFollow(listUser)
		elif etat == "homeProfil":
			user = self.parent.parent.getUserInfo(self.userId)
			listTweet = self.parent.parent.getAllTweets(self.userId)
			following = self.parent.parent.isFollowing(self.userId)
			self.homeProfilView.update()
			self.homeProfilView.addContent(listTweet,following,user)
			
		
	def showHome(self):
		self.enableUpdateButton()
		user = self.parent.parent.getUserInfo(None)
		listTweet = self.parent.parent.getAllTweets(None)
		self.hideLastView("home")
		self.homeView.display()
		self.homeView.addContent(listTweet,user)
		self.parent.state = "home"
		
	def showTweet(self):
		self.enableUpdateButton()
		user = self.parent.parent.getUserInfo(None)
		listTweet = self.parent.parent.getFollowingMessages(None)
		self.hideLastView("tweet")
		self.tweetView.display()
		self.tweetView.addTweets(listTweet,user)
		self.parent.state = "tweet"
		
	def showFollower(self):
		self.disableUpdateButton()
		listUser = self.parent.parent.getFollower()
		self.hideLastView("follower")
		self.followView.display()
		self.followView.addFollow(listUser)
		self.parent.state = "follower"
		
	def showFollowing(self):
		self.disableUpdateButton()
		listUser = self.parent.parent.getFollowing()
		self.hideLastView("following")
		self.followView.display()
		self.followView.addFollow(listUser)
		self.parent.state = "following"
	
	def showProfil(self):
		self.disableUpdateButton()
		user = self.parent.parent.getUserInfo(None)
		self.hideLastView("profil")
		self.profilView.display()
		self.profilView.addProfilInfo(user)
		self.parent.state = "profil"
	
	def showSearch(self):
		self.disableUpdateButton()
		self.hideLastView("search")
		self.searchView.display()
		self.parent.state = "search"
	
	def showHomeProfil(self,userId):
		self.userId = userId
		self.enableUpdateButton()
		user = self.parent.parent.getUserInfo(userId)
		listTweet = self.parent.parent.getAllTweets(userId)
		self.hideLastView("homeProfil")
		self.homeProfilView.display(userId)
		following = self.parent.parent.isFollowing(userId)
		self.homeProfilView.addContent(listTweet,following,user)
		self.parent.state = "homeProfil"
	
	def hideLastView(self, next):
		if self.parent.state == "home" and next != "home":
			self.homeView.hide()
		elif self.parent.state == "tweet" and next != "tweet":
			self.tweetView.hide()
		elif self.parent.state == "follower" and next != "follower" or self.parent.state == "following" and next != "following":
			self.followView.hide()
		elif self.parent.state == "profil":
			self.profilView.hide()
		elif self.parent.state == "search":
			self.searchView.hide()
		elif self.parent.state == "homeProfil":
			self.homeProfilView.hide()
	
	def checkLength(self,event):
		text = self.textTweet.get("1.0",END)
		if len(text) >= 139:
			self.textTweet.delete("1.0",END)
			self.textTweet.insert(1.0,text[:139])
	
	def tweet(self):
		self.checkLength(None)
		self.parent.parent.sendTweet(self.textTweet.get("1.0",END))
		self.update()
		self.textTweet.delete("1.0",END)
	
	def updateUser(self,user):
		self.parent.parent.updateUser(user)
	
	def disconnect(self):
		self.showHome();
		self.parent.parent.disconnectUser()
		self.hide()
		self.parent.state="login"
		self.parent.displayLogin()

	def deleteTweetById(self, id):
		self.parent.parent.deleteTweetById(id)
		self.update()

	def updateTweet(self, id, msg):
		self.parent.parent.updateTweet(id, msg)
		self.update()
	
	def searchUser(self,name):
		return self.parent.parent.searchUser(name)
	
	def follow(self,userId):
		self.parent.parent.follow(userId)
		
	def unfollow(self,userId):
		self.parent.parent.unfollow(userId)
	
	def getId(self):
		return self.parent.parent.getId()
	
	def disableUpdateButton(self):
		self.refreshButton.pack_forget()
	
	def enableUpdateButton(self):
		self.refreshButton.pack(pady=20,side=BOTTOM)
	