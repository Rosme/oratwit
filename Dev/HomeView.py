from tkinter import * 
from Tweet import *

class HomeView:
	def __init__(self,parent):
		self.parent = parent
		self.im = PhotoImage(file="Assets/egg.gif")
		
		self.homeViewFrame = Canvas(self.parent.tweetContainerFrame,width=850,height=740)
		self.scrollbarY = Scrollbar(self.parent.tweetContainerFrame,command=self.homeViewFrame.yview)
		self.homeViewFrame.config(yscrollcommand=self.scrollbarY.set)
		
		self.scrollbarX = Scrollbar(self.parent.tweetContainerFrame,command=self.homeViewFrame.xview,orient=HORIZONTAL)
		self.homeViewFrame.config(xscrollcommand=self.scrollbarX.set)
		
		self.updatableFrame = Frame(self.homeViewFrame)
		
	def display(self):
		self.update()
		self.scrollbarY.pack(side=RIGHT,fill=Y)
		self.scrollbarX.pack(side=BOTTOM,fill=X)
		self.homeViewFrame.pack(expand=1)
		
	def hide(self):
		self.homeViewFrame.pack_forget()
		self.scrollbarY.pack_forget()
		self.scrollbarX.pack_forget()
		
	def update(self):
		self.clean()
		
		self.updatableFrame = Frame(self.homeViewFrame)
		self.window = self.homeViewFrame.create_window(0,0,window=self.updatableFrame,anchor="nw")
		
		self.profilFrame = Frame(self.updatableFrame)
		self.profilFrame.pack(expand=1,fill=BOTH, pady=(0,10))
		
		self.listTweetFrame = Frame(self.updatableFrame)
		self.listTweetFrame.pack(expand=1,fill=BOTH)
		
	def clean(self):
		self.updatableFrame.destroy()
	
	def addContent(self,listTweet,user):
		self.addInfo(user)
		self.addTweets(listTweet,user)
	
	def addTweets(self,listTweet,user):
		id = self.parent.getId()
		for tweet in listTweet:
			t = Tweet(self,self.listTweetFrame,tweet,user,id,pady=5,padx=10)
			t.pack(expand=1,fill=Y)
		
		self.listTweetFrame.update_idletasks()
		self.homeViewFrame.configure(scrollregion=(0,0,self.listTweetFrame.winfo_reqwidth(),self.listTweetFrame.winfo_reqheight()+120))
		
	def addInfo(self,user):
		canva = Canvas(self.profilFrame,width=100,height=100)
		canva.create_image(50,50,image=self.im)
		canva.grid(column=0,row=0,padx=(23,5))

		self.infoFrame = Frame(self.profilFrame)
		self.infoFrame.grid(column=1, row=0)
		self.infoFrame.grid_columnconfigure(1, minsize=125)

		usernameLabel = Label(self.infoFrame,text=user["username"], font=('Arial', 17, 'bold'))
		usernameLabel.grid(column=0,row=0,sticky=W+N)
		nameLabel = Label(self.infoFrame,text=(user["firstname"] + " " + user["lastname"]), font=('Arial', 10, 'italic'))
		nameLabel.grid(column=0,row=1,sticky=W)
		locationLabel = Label(self.infoFrame,text=user["country"], font=('Arial', 8))
		locationLabel.grid(column=0,row=2,sticky=W)
		emailLabel = Label(self.infoFrame,text=user["email"], font=('Arial', 7))
		emailLabel.grid(column=0,row=3,sticky=W)
		
		biography = Text(self.profilFrame, width=58, height=6, font=('Arial', 8))
		if user["biography"] != None:
			biography.insert(1.0,user["biography"])
		biography.config(state=DISABLED)
		biography.grid(column=2,row=0,sticky=E,padx=15,pady=5)
	
	def updateTweet(self,info):
		self.parent.updateTweet(info["id"], info["message"])
