import Modele
import Vue

class Controleur():
	def __init__(self):
		self.modele = Modele.Modele(self)
		self.vue = Vue.Vue(self)
		
		self.vue.update()
		self.showLogin()
		self.vue.display()

	def showLogin(self):
		self.vue.displayLogin()
		
	def authenticate(self, username, password):
		sucess = self.modele.authenticate(username, password)
		return sucess
	
	def createUser(self, username, firstname, lastname, password):
		return self.modele.createUser(username, firstname, lastname, password)
	
	def getFollower(self):
		return self.modele.getFollowerUsername()
		
	def getFollowing(self):
		return self.modele.getFollowingUsername()
		
	def getFollowingMessages(self):
		return self.modele.getFollowingMessages()
		
	def getUserInfo(self):
		return self.modele.getUserInfo()
		
	def sendTweet(self,message):
		self.modele.sendTweet(message)
		
	def updateUser(self,user):
		return self.modele.updateUser(user)
	
	def disconnectUser(self):
		self.modele.disconnectUser()