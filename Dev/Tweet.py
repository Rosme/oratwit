from tkinter import *
from UpdateTweetPrompt import *

class Tweet(Frame):
	def __init__(self,parent,parentFrame,info,user,id,**options):
		Frame.__init__(self,parentFrame,options)
		self.im = PhotoImage(file="Assets/egg.gif")
		
		self.parent = parent
		self.info = info
		self.configure(borderwidth=2, relief=GROOVE)
		
		canva = Canvas(self,width=100,height=100)
		canva.create_image(50,50,image=self.im)
		canva.grid(column=0,row=0,rowspan=2,padx=5)
		
		subFrame = Frame(self)
		subFrame.grid(column=1,row=0,sticky=EW)
		subFrame.grid_columnconfigure(0,minsize=672)
		
		label = Label(subFrame,text=(self.info["username"] + " - " + self.info["firstname"] + " " + self.info["lastname"]),font=('Arial',"9","bold"))
		label.grid(column=0,row=0,sticky=W+N)
		label.bind('<Button-1>',self.seeProfil)
		
		self.modifyB = Button(subFrame,text="M", font=('Arial', 5))
		self.modifyB.grid(column=1,row=0,sticky=E)
		
		self.deleteB = Button(subFrame,text="X", font=('Arial', 5))
		self.deleteB.grid(column=2,row=0,sticky=E)
		
		if self.info['id_user'] != id:
			self.modifyB.config(state=DISABLED,relief=FLAT,text=" ")
			self.deleteB.config(state=DISABLED,relief=FLAT,text=" ")
		else:
			self.modifyB.bind('<Button-1>',self.updatemessage)
			self.deleteB.bind('<Button-1>',self.deleteMessage)
			self.configure(padx=6)
		
		date = Label(subFrame,text=self.info["date"], font=('Arial', 8))
		date.grid(column=0,row=0,sticky=E+N, padx=(0, 20), columnspan=2)
		
		text = Text(self,height=3,width=18, font=('Arial', 8))
		text.insert(1.0,self.info["message"])
		text.config(state=DISABLED)
		text.grid(column=1,row=1,sticky=N+E+W)
	
	def deleteMessage(self,event):
		id = self.info['id']
		self.parent.parent.deleteTweetById(id)
		
	def updatemessage(self,event):
		self.updateTweetPrompt = UpdateTweetPrompt(self,self.info)
	
	def seeProfil(self,event):
		self.parent.parent.showHomeProfil(self.info['id_user'])
		
