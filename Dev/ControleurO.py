# -*- coding: utf-8 -*-
import ModeleO
import Connection
import Vue

class Controleur():
	def __init__(self):
		self.connection = Connection.Connection()
		self.connection.connect()
		self.modele = ModeleO.Modele(self, self.connection.getConnection())
		self.vue = Vue.Vue(self)
		
		self.showLogin()
		self.vue.display()
		self.connection.disconnect()
		
	def authenticate(self, username, password):
		return self.modele.authenticate(username, password)
		
	def createUser(self, username, firstname, lastname, password):
		return self.modele.createUser(username, firstname, lastname, password)

	def setCreateUserErrors(self, message):
		self.message = message

	def getAllTweets(self, userId):
		return self.modele.getAllTweets(userId)
		
	def getCreateUserErrors(self):
		return self.message
		
	def getFollower(self):
		return self.modele.getFollowerUsername()
		
	def getFollowing(self):
		return self.modele.getFollowingUsername()
		
	def getFollowingMessages(self, userId):
		return self.modele.getFollowingMessages()
		
	def getUserInfo(self, userId):
		return self.modele.getUserInfo(userId)
		
	def showLogin(self):
		self.vue.displayLogin()
	
	def updateUser(self,user):
		return self.modele.updateUser(user)
	
	def deleteUser(self):
		self.modele.deleteUser()

	def disconnectUser(self):
		self.modele.disconnectUser()
	
	def sendTweet(self,message):
		self.modele.sendTweet(message)

	def deleteTweetById(self, id):
		self.modele.deleteTweet(id)

	def updateTweet(self, id, message):
		self.modele.modifyTweet(id, message)
	
	def searchUser(self,name):
		return self.modele.searchUser(name)
	
	def isFollowing(self,userId):
		return self.modele.isFollowing(userId)
	
	def follow(self,userId):
		self.modele.addFollowingUser(userId)
		
	def unfollow(self,userId):
		self.modele.unfollow(userId)
	
	def getId(self):
		return self.modele.getId()
	
if __name__ == '__main__':
    c = Controleur()